let main = document.querySelector(".main-container");

let row = 8;
let cloumn = 8;
for (let i = 1; i <= row; i++) {
  for (let j = 1; j <= cloumn; j++) {
    let box = document.createElement("div");
    box.className = `box${i}${j} box`;
    main.appendChild(box);
  }
}
let boxes = document.querySelectorAll(".box");
boxes.forEach((box) => {
  let className = box.className;
  let process = className.match(/\d+/)[0];
  let number = process.split("").reduce((a, b) => {
    return Number(a) + Number(b);
  });
  if (number % 2 !== 0) {
    box.classList.add("black");
  }
});
let removeRedClass = () => {
  let RedClass = document.querySelectorAll(".red");
  RedClass.forEach((redBox) => {
    redBox.classList.remove("red");
  });
};
let backgroundColor = (e) => {
  removeRedClass();
  let target = e.target;
  let className = target.className;
  let process = className.match(/\d+/)[0];
  let number = process.split("");
  let a = Number(number[0]);
  let b = Number(number[1]);
  diagonalRedBackground(a, b);
};
main.addEventListener("click", backgroundColor);

let diagonalRedBackground = (a, b) => {
  let j = b;
  for (let i = a; i > 0; i--) {
    if (j > 0) {
      let box = document.querySelector(`.box${i}${j}`);
      box.classList.add("red");
      --j;
    }
  }
  j = b;
  for (let i = a; i < 9; i++) {
    if (j < 9) {
      let box = document.querySelector(`.box${i}${j}`);
      box.classList.add("red");
      ++j;
    }
  }
  j = b;
  for (let i = a; i > 0; i--) {
    if (j < 9) {
      let box = document.querySelector(`.box${i}${j}`);
      box.classList.add("red");
      ++j;
    }
  }
  j = b;
  for (let i = a; i < 9; i++) {
    if (j > 0) {
      let box = document.querySelector(`.box${i}${j}`);
      box.classList.add("red");
      --j;
    }
  }
};
